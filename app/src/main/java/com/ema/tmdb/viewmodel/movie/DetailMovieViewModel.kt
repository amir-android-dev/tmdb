package com.ema.tmdb.viewmodel.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.tmdb.data.model.network.ResponsePopularDetail
import com.ema.tmdb.data.repository.popularmovie.PopularMovieRepo
import com.ema.tmdb.utils.MyNetworkRequest
import com.ema.tmdb.utils.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(private val repo: PopularMovieRepo) : ViewModel() {
    private val _detail = MutableLiveData<MyNetworkRequest<ResponsePopularDetail>>()
    val detail get(): LiveData<MyNetworkRequest<ResponsePopularDetail>> = _detail

    fun callDetail(id: Int) = viewModelScope.launch {
        _detail.value = MyNetworkRequest.Loading()
        val response = repo.callPopularDetail(id)
        _detail.value = NetworkResponse(response).generalNetworkResponse()


    }


}