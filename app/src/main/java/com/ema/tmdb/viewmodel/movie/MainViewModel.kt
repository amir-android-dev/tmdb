package com.ema.tmdb.viewmodel.movie

import android.util.Log
import androidx.lifecycle.*
import com.ema.tmdb.data.model.network.ResponseNowPlaying
import com.ema.tmdb.data.model.network.ResponsePopularMovies
import com.ema.tmdb.data.model.network.ResponseTopRatedMovies
import com.ema.tmdb.data.repository.main.MainRepo
import com.ema.tmdb.utils.MyNetworkRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repo: MainRepo,
) : ViewModel() {


    //popular
    private var _responsePopular = MutableLiveData<MyNetworkRequest<ResponsePopularMovies>>()
    val responsePopular: LiveData<MyNetworkRequest<ResponsePopularMovies>>
        get() = _responsePopular
    val popularMoviesDb = repo.getPopularMovies().asLiveData(Dispatchers.IO)

    //top rated
    private var _responseTopRated = MutableLiveData<MyNetworkRequest<ResponseTopRatedMovies>>()
    val responseTopRated: LiveData<MyNetworkRequest<ResponseTopRatedMovies>>
        get() = _responseTopRated

    val topRatedDb = repo.getTopRatedMovies().asLiveData(Dispatchers.IO)

    //now playing
    private var _responseNowPlaying = MutableLiveData<MyNetworkRequest<ResponseNowPlaying>>()
    val responseNowPlaying: LiveData<MyNetworkRequest<ResponseNowPlaying>>
        get() = _responseNowPlaying

    val nowPlayingMoviesDb = repo.getNowPlayingMovies().asLiveData(Dispatchers.IO)

    //up coming
    private fun loadUpComing() = viewModelScope.launch(Dispatchers.IO) {
        repo.callUpComingMovies()
    }

    val upComingMoviesDb = repo.getUpComingMovies().asLiveData(Dispatchers.IO)

    private fun loadPopular() = viewModelScope.launch(Dispatchers.IO) {
        val response = repo.callPopularMovies()
        _responsePopular.postValue(response)
    }


    private fun loadTopRated() = viewModelScope.launch(Dispatchers.IO) {
        val response = repo.callTopRated()
        _responseTopRated.postValue(response)

    }


    private fun loadNowPlaying() = viewModelScope.launch(Dispatchers.IO) {
        val response = repo.callNowPlayingMovies()
        _responseNowPlaying.postValue(response)
    }

    fun refresh() {
        loadPopular()
        loadNowPlaying()
        loadTopRated()
        loadUpComing()
    }

}