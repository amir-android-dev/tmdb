package com.ema.tmdb.viewmodel.movie


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.data.repository.favorite.FavoriteRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(private val repo: FavoriteRepo) : ViewModel() {

    val getAllFavoriteMovies = repo.getFavoriteMovies().asLiveData()
    private val _favoriteExists = MutableLiveData<Boolean>()
    val favoriteExists get(): LiveData<Boolean> = _favoriteExists


    fun existsFavoriteItem(id: Int) = viewModelScope.launch {
        repo.existsFavorite(id).collect {
            _favoriteExists.postValue(it)
        }
    }

    fun insertFavorite(entity: EntityFavoriteMovies) =
        viewModelScope.launch { repo.insertFavoriteMovie(entity) }

    fun deleteFavorite(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repo.deleteFavorite(id)
    }
}
