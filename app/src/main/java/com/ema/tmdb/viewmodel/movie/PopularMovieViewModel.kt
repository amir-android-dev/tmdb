package com.ema.tmdb.viewmodel.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.ema.tmdb.data.paging.PagingPopularSource
import com.ema.tmdb.data.repository.popularmovie.PopularMovieRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PopularMovieViewModel @Inject constructor(private val repo: PopularMovieRepo) : ViewModel() {

    val popularMoviesList = Pager(PagingConfig(10)) {
        PagingPopularSource(repo)
    }.flow.cachedIn(viewModelScope)
}