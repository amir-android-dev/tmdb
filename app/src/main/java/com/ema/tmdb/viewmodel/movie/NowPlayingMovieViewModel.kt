package com.ema.tmdb.viewmodel.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.ema.tmdb.data.paging.PagingNowPlayingSource
import com.ema.tmdb.data.repository.nowplaying.NowPlayingRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NowPlayingMovieViewModel @Inject constructor(private val repo: NowPlayingRepo) : ViewModel() {

    val nowPlayingMovieList = Pager(PagingConfig(10)) {
        PagingNowPlayingSource(repo)
    }.flow.cachedIn(viewModelScope)
}