package com.ema.tmdb.viewmodel.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.ema.tmdb.data.paging.PagingTopRatedMoviesSource
import com.ema.tmdb.data.repository.toprated.TopRatedVideosRepo
import com.ema.tmdb.utils.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import javax.inject.Inject

@HiltViewModel
class TopRatedMoviesViewModel @Inject constructor(private val repo: TopRatedVideosRepo) :
    ViewModel() {

    private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    val topRatedMovies = Pager(PagingConfig(10)) {
        PagingTopRatedMoviesSource(repo, _errorMessage)
    }.flow.cachedIn(viewModelScope).catch { e ->
        Throwable(e.message)

    }

}