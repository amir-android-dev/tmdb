package com.ema.tmdb.data.repository.popularmovie

import com.ema.tmdb.data.model.network.ResponsePopularDetail
import com.ema.tmdb.data.model.network.ResponsePopularMovies
import retrofit2.Response

interface PopularMovieRepo {
    suspend fun callPopularMovies(page: Int): Response<ResponsePopularMovies>
    suspend fun callPopularDetail(id: Int): Response<ResponsePopularDetail>
}