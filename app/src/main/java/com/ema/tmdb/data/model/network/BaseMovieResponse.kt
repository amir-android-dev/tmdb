package com.ema.tmdb.data.model.network

sealed class BaseMovieResponse {
    abstract val id: Int?
    abstract val title: String?
    abstract val posterPath: String?
    abstract val releaseDate: String?
    abstract val voteAverage: Double?
}

data class ResponsesNowPlaying(
    override val id: Int?,
    override val title: String?,
    override val posterPath: String?,
    override val releaseDate: String?,
    override val voteAverage: Double?

) : BaseMovieResponse()

data class ResponsesPopularMovies(
    override val id: Int?,
    override val title: String?,
    override val posterPath: String?,
    override val releaseDate: String?,
    override val voteAverage: Double?
    // your properties here
) : BaseMovieResponse()

data class ResponsesTopRatedMovies(
    override val id: Int?,
    override val title: String?,
    override val posterPath: String?,
    override val releaseDate: String?,
    override val voteAverage: Double?
    // your properties here
) : BaseMovieResponse()
