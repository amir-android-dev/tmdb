package com.ema.tmdb.data.paging

import android.annotation.SuppressLint
import android.icu.text.SimpleDateFormat
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ema.tmdb.data.model.network.ResponseNowPlaying.Result
import com.ema.tmdb.data.repository.nowplaying.NowPlayingRepo
import java.util.*

import javax.inject.Inject

class PagingNowPlayingSource @Inject constructor(private val repo: NowPlayingRepo) :
    PagingSource<Int, Result>() {
    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        return null
    }

    @SuppressLint("SimpleDateFormat")
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        return try {
            val currentPage = params.key ?: 1
            val response = repo.callNowPlayingMovie(currentPage)
            val nowPlayingMovies = response.body()!!.results ?: emptyList()

            //date
            val nowPlayingMoviesDate = response.body()!!.dates
            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            val currentDate = org.threeten.bp.LocalDate.now()
            val maximumDate = dateFormat.parse(nowPlayingMoviesDate!!.minimum)
            val min = currentDate.minusMonths(1)
            val calendar = Calendar.getInstance()
            calendar.time = maximumDate
            calendar.add(Calendar.MONTH, -1)
            val oneMonthBeforeMaxDate = calendar.time
            val responseNowPlayingMovies = mutableListOf<Result>()


            //end
            /*    val responseNowPlayingMovies = mutableListOf<Result>()
                responseNowPlayingMovies.addAll(nowPlayingMovies)*/
            if (response.isSuccessful ) {
                responseNowPlayingMovies.addAll(nowPlayingMovies)
                LoadResult.Page(
                    data = responseNowPlayingMovies,
                    prevKey = if (currentPage == 1) null else currentPage.minus(1),
                    nextKey = if (response.body()!!.results!!.isNotEmpty()) currentPage.plus(1) else null
                )
            } else {
                LoadResult.Error(Throwable(response.message()))
            }
        } catch (e: Exception) {
            LoadResult.Error(Throwable(e.message))
        }
    }
}