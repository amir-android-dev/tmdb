package com.ema.tmdb.data.repository.toprated

import com.ema.tmdb.data.model.network.ResponseTopRatedMovies
import retrofit2.Response

interface TopRatedVideosRepo {
    suspend fun callTopRatedMovies(page: Int): Response<ResponseTopRatedMovies>
}