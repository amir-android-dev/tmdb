package com.ema.tmdb.data.resource


import com.ema.tmdb.data.local.DaoTMDB
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.data.model.network.ResponseUpComingMovies
import com.ema.tmdb.data.network.ApiServices
import com.ema.tmdb.utils.Constants.API_KEY_VALUE
import kotlinx.coroutines.flow.Flow
import retrofit2.Response


import javax.inject.Inject

class ResourceImpl @Inject constructor(private val api: ApiServices, private val dao: DaoTMDB) :
    MyResource {

    //popular
    override suspend fun callPopularMovies(page: Int) =
        api.getPopularMovie(API_KEY_VALUE, page)

    //detail
    override suspend fun callPopularDetail(id: Int) =
        api.getPopularDetail(id = id, apiKey = API_KEY_VALUE)

    //now playing
    override suspend fun callNowPlayingMovie(page: Int) =
        api.getNowPlayingMovie(API_KEY_VALUE, page)

    //top rated
    override suspend fun callTopRatedMovies(page: Int) =
        api.getTopRatedMovies(API_KEY_VALUE, page)

    //up coming
    override suspend fun callUpComingMovies(page: Int) = api.getUpComingMovies(API_KEY_VALUE, page)

    //db
    override suspend fun insertMovie(entity: EntityMovies) = dao.insertMovie(entity)
    override fun getPopularMovies(): Flow<List<EntityMovies>> = dao.getPopularMovies()
    override fun getTopRatedMovies(): Flow<List<EntityMovies>> = dao.getTopRatedMovies()
    override fun getNowPlayingMovies(): Flow<List<EntityMovies>> = dao.getNowPlayingMovies()
    override fun getUpComingMovies(): Flow<List<EntityMovies>> = dao.getUpComingMovies()

    //db favorite
    override suspend fun insertFavoriteMovie(entity: EntityFavoriteMovies) =
        dao.insertFavorite(entity)

    override fun getFavoriteMovies(): Flow<List<EntityFavoriteMovies>> = dao.getFavoriteMovies()
    override fun existsFavorite(id: Int): Flow<Boolean> = dao.existsFavorite(id)
    override suspend fun deleteFavorite(id: Int) = dao.deleteFavorite(id)

}
//        try {
//            api.getPopular(apiKey, page)
//        } catch (e: Exception) {
//            null
//        }