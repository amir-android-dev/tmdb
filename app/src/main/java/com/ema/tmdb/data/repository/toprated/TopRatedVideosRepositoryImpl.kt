package com.ema.tmdb.data.repository.toprated


import com.ema.tmdb.data.resource.MyResource

import javax.inject.Inject

class TopRatedVideosRepositoryImpl @Inject constructor(private val res: MyResource) :
    TopRatedVideosRepo {
    override suspend fun callTopRatedMovies(page: Int) = res.callTopRatedMovies(page)
}