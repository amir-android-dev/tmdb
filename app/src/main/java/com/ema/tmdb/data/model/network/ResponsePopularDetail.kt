package com.ema.tmdb.data.model.network


import com.google.gson.annotations.SerializedName

data class ResponsePopularDetail(
    @SerializedName("adult")
    val adult: Boolean?, // false
    @SerializedName("backdrop_path")
    val backdropPath: String?, // /gMJngTNfaqCSCqGD4y8lVMZXKDn.jpg
    @SerializedName("belongs_to_collection")
    val belongsToCollection: BelongsToCollection?,
    @SerializedName("budget")
    val budget: Int?, // 200000000
    @SerializedName("genres")
    val genres: List<Genre>?,
    @SerializedName("homepage")
    val homepage: String?, // https://www.marvel.com/movies/ant-man-and-the-wasp-quantumania
    @SerializedName("id")
    val id: Int?, // 640146
    @SerializedName("images")
    val images: Images?,
    @SerializedName("imdb_id")
    val imdbId: String?, // tt10954600
    @SerializedName("original_language")
    val originalLanguage: String?, // en
    @SerializedName("original_title")
    val originalTitle: String?, // Ant-Man and the Wasp: Quantumania
    @SerializedName("overview")
    val overview: String?, // Super-Hero partners Scott Lang and Hope van Dyne, along with with Hope's parents Janet van Dyne and Hank Pym, and Scott's daughter Cassie Lang, find themselves exploring the Quantum Realm, interacting with strange new creatures and embarking on an adventure that will push them beyond the limits of what they thought possible.
    @SerializedName("popularity")
    val popularity: Double?, // 8567.865
    @SerializedName("poster_path")
    val posterPath: String?, // /ngl2FKBlU4fhbdsrtdom9LVLBXw.jpg
    @SerializedName("production_companies")
    val productionCompanies: List<ProductionCompany>?,
    @SerializedName("production_countries")
    val productionCountries: List<ProductionCountry>?,
    @SerializedName("release_date")
    val releaseDate: String?, // 2023-02-15
    @SerializedName("revenue")
    val revenue: Long?, // 473237851
    @SerializedName("runtime")
    val runtime: Int?, // 125
    @SerializedName("spoken_languages")
    val spokenLanguages: List<SpokenLanguage?>?,
    @SerializedName("status")
    val status: String?, // Released
    @SerializedName("tagline")
    val tagline: String?, // Witness the beginning of a new dynasty.
    @SerializedName("title")
    val title: String?, // Ant-Man and the Wasp: Quantumania
    @SerializedName("video")
    val video: Boolean?, // false
    @SerializedName("videos")
    val videos: Videos?,
    @SerializedName("vote_average")
    val voteAverage: Double?, // 6.525
    @SerializedName("vote_count")
    val voteCount: Int? // 1898
) {
    data class BelongsToCollection(
        @SerializedName("backdrop_path")
        val backdropPath: String?, // /2KjtWUBiksmN8LsUouaZnxocu5N.jpg
        @SerializedName("id")
        val id: Int?, // 422834
        @SerializedName("name")
        val name: String?, // Ant-Man Collection
        @SerializedName("poster_path")
        val posterPath: String? // /qxMs5TU6zOTQ7cbmBZ6xGvHUDa2.jpg
    )

    data class Genre(
        @SerializedName("id")
        val id: Int?, // 28
        @SerializedName("name")
        val name: String? // Action
    )

    data class Images(
        @SerializedName("backdrops")
        val backdrops: List<Backdrop?>?,
        @SerializedName("logos")
        val logos: List<Logo?>?,
        @SerializedName("posters")
        val posters: List<Poster?>?
    ) {
        data class Backdrop(
            @SerializedName("aspect_ratio")
            val aspectRatio: Double?, // 1.778
            @SerializedName("file_path")
            val filePath: String?, // /gMJngTNfaqCSCqGD4y8lVMZXKDn.jpg
            @SerializedName("height")
            val height: Int?, // 2160
            @SerializedName("iso_639_1")
            val iso6391: String?, // en
            @SerializedName("vote_average")
            val voteAverage: Double?, // 5.388
            @SerializedName("vote_count")
            val voteCount: Int?, // 4
            @SerializedName("width")
            val width: Int? // 3840
        )

        data class Logo(
            @SerializedName("aspect_ratio")
            val aspectRatio: Double?, // 2.55
            @SerializedName("file_path")
            val filePath: String?, // /caLr9B3pbN939I53KaJD1vu9Yqe.png
            @SerializedName("height")
            val height: Int?, // 725
            @SerializedName("iso_639_1")
            val iso6391: String?, // zh
            @SerializedName("vote_average")
            val voteAverage: Double?, // 5.384
            @SerializedName("vote_count")
            val voteCount: Int?, // 2
            @SerializedName("width")
            val width: Int? // 1849
        )

        data class Poster(
            @SerializedName("aspect_ratio")
            val aspectRatio: Double?, // 0.667
            @SerializedName("file_path")
            val filePath: String?, // /ngl2FKBlU4fhbdsrtdom9LVLBXw.jpg
            @SerializedName("height")
            val height: Int?, // 3000
            @SerializedName("iso_639_1")
            val iso6391: String?, // en
            @SerializedName("vote_average")
            val voteAverage: Double?, // 4.97
            @SerializedName("vote_count")
            val voteCount: Int?, // 38
            @SerializedName("width")
            val width: Int? // 2000
        )
    }

    data class ProductionCompany(
        @SerializedName("id")
        val id: Int?, // 420
        @SerializedName("logo_path")
        val logoPath: String?, // /hUzeosd33nzE5MCNsZxCGEKTXaQ.png
        @SerializedName("name")
        val name: String?, // Marvel Studios
        @SerializedName("origin_country")
        val originCountry: String? // US
    )

    data class ProductionCountry(
        @SerializedName("iso_3166_1")
        val iso31661: String?, // US
        @SerializedName("name")
        val name: String? // United States of America
    )

    data class SpokenLanguage(
        @SerializedName("english_name")
        val englishName: String?, // English
        @SerializedName("iso_639_1")
        val iso6391: String?, // en
        @SerializedName("name")
        val name: String? // English
    )

    data class Videos(
        @SerializedName("results")
        val results: List<Result?>?
    ) {
        data class Result(
            @SerializedName("id")
            val id: String?, // 644b44be336e0105417332a7
            @SerializedName("iso_3166_1")
            val iso31661: String?, // US
            @SerializedName("iso_639_1")
            val iso6391: String?, // en
            @SerializedName("key")
            val key: String?, // UJZx8MayWxk
            @SerializedName("name")
            val name: String?, // Streaming May 17 on Disney+
            @SerializedName("official")
            val official: Boolean?, // true
            @SerializedName("published_at")
            val publishedAt: String?, // 2023-04-27T16:18:10.000Z
            @SerializedName("site")
            val site: String?, // YouTube
            @SerializedName("size")
            val size: Int?, // 1080
            @SerializedName("type")
            val type: String? // Teaser
        )
    }
}