package com.ema.tmdb.data.repository.favorite

import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.utils.MyNetworkRequest
import com.ema.tmdb.utils.NetworkResponse
import kotlinx.coroutines.flow.Flow

interface FavoriteRepo {
    //db favorite
    suspend fun insertFavoriteMovie(entity: EntityFavoriteMovies)
    fun getFavoriteMovies(): Flow<List<EntityFavoriteMovies>>

    fun existsFavorite(id: Int): Flow<Boolean>
    suspend fun deleteFavorite(id: Int)
}