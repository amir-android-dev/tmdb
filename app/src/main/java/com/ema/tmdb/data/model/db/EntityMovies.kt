package com.ema.tmdb.data.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ema.tmdb.utils.Constants.TABLE_MOVIE

@Entity(tableName = TABLE_MOVIE)
data class EntityMovies(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val posterPath: String?,
    val voteAverage: Double?,
    val releaseDate: String?,
    val originalTitle: String?,
    val category :String
)
