package com.ema.tmdb.data.repository.nowplaying

import com.ema.tmdb.data.resource.MyResource
import javax.inject.Inject

class NowPlayingRepoImpl @Inject constructor(private val resource: MyResource) : NowPlayingRepo {
    override suspend fun callNowPlayingMovie(page: Int) = resource.callNowPlayingMovie(page)
}