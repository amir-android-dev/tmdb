package com.ema.tmdb.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.data.model.db.EntityMovies

@Database(
    entities = [EntityMovies::class, EntityFavoriteMovies::class],
    version = 1,
    exportSchema = true
)
abstract class DatabaseTMDB : RoomDatabase() {
    abstract fun dao(): DaoTMDB
}