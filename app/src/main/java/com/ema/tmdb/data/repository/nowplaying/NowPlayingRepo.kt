package com.ema.tmdb.data.repository.nowplaying

import com.ema.tmdb.data.model.network.ResponseNowPlaying
import retrofit2.Response

interface NowPlayingRepo {
    suspend fun callNowPlayingMovie(
        page: Int
    ): Response<ResponseNowPlaying>
}