package com.ema.tmdb.data.model.network


import com.google.gson.annotations.SerializedName

data class ResponseUpComingMovies(
    @SerializedName("dates")
    val dates: Dates?,
    @SerializedName("page")
    val page: Int?, // 1
    @SerializedName("results")
    val results: List<Result>?,
    @SerializedName("total_pages")
    val totalPages: Int?, // 19
    @SerializedName("total_results")
    val totalResults: Int? // 378
) {
    data class Dates(
        @SerializedName("maximum")
        val maximum: String?, // 2023-06-07
        @SerializedName("minimum")
        val minimum: String? // 2023-05-17
    )

    data class Result(
        @SerializedName("adult")
        val adult: Boolean?, // false
        @SerializedName("backdrop_path")
        val backdropPath: String?, // /iJQIbOPm81fPEGKt5BPuZmfnA54.jpg
        @SerializedName("genre_ids")
        val genreIds: List<Int?>?,
        @SerializedName("id")
        val id: Int?, // 502356
        @SerializedName("original_language")
        val originalLanguage: String?, // en
        @SerializedName("original_title")
        val originalTitle: String?, // The Super Mario Bros. Movie
        @SerializedName("overview")
        val overview: String?, // While working underground to fix a water main, Brooklyn plumbers—and brothers—Mario and Luigi are transported down a mysterious pipe and wander into a magical new world. But when the brothers are separated, Mario embarks on an epic quest to find Luigi.
        @SerializedName("popularity")
        val popularity: Double?, // 3014.97
        @SerializedName("poster_path")
        val posterPath: String?, // /qNBAXBIQlnOThrVvA6mA2B5ggV6.jpg
        @SerializedName("release_date")
        val releaseDate: String?, // 2023-04-05
        @SerializedName("title")
        val title: String?, // The Super Mario Bros. Movie
        @SerializedName("video")
        val video: Boolean?, // false
        @SerializedName("vote_average")
        val voteAverage: Double?, // 7.5
        @SerializedName("vote_count")
        val voteCount: Int? // 1901
    )
}