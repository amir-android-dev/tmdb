package com.ema.tmdb.data.resource

import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.data.model.network.*
import kotlinx.coroutines.flow.Flow
import retrofit2.Response


interface MyResource {
    //popular
    suspend fun callPopularMovies(page: Int): Response<ResponsePopularMovies>


    suspend fun callPopularDetail(id: Int): Response<ResponsePopularDetail>

    //now playing
    suspend fun callNowPlayingMovie(page: Int): Response<ResponseNowPlaying>

    //top rated
    suspend fun callTopRatedMovies(page: Int): Response<ResponseTopRatedMovies>

    //up coming
    suspend fun callUpComingMovies(page: Int): Response<ResponseUpComingMovies>

    //db
    suspend fun insertMovie(entity: EntityMovies)
    fun getPopularMovies(): Flow<List<EntityMovies>>
    fun getTopRatedMovies(): Flow<List<EntityMovies>>
    fun getNowPlayingMovies(): Flow<List<EntityMovies>>
    fun getUpComingMovies(): Flow<List<EntityMovies>>

    //db favorite
    suspend fun insertFavoriteMovie(entity: EntityFavoriteMovies)
    fun getFavoriteMovies(): Flow<List<EntityFavoriteMovies>>
    fun existsFavorite(id: Int): Flow<Boolean>
    suspend fun deleteFavorite(id: Int)


}