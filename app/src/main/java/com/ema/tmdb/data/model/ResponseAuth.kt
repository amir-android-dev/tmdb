package com.ema.tmdb.data.model


import com.google.gson.annotations.SerializedName

data class ResponseAuth(
    @SerializedName("expires_at")
    val expiresAt: String?, // 2023-04-24 16:22:45 UTC
    @SerializedName("request_token")
    val requestToken: String?, // 8d4feb06fd3d5255933bdfac34e092f20bcc5851
    @SerializedName("success")
    val success: Boolean? // true
)