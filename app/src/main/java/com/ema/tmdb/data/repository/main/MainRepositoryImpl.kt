package com.ema.tmdb.data.repository.main

import android.util.Log
import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.data.model.network.ResponseNowPlaying
import com.ema.tmdb.data.model.network.ResponsePopularMovies
import com.ema.tmdb.data.model.network.ResponseTopRatedMovies
import com.ema.tmdb.data.model.network.ResponseUpComingMovies
import com.ema.tmdb.data.resource.MyResource
import com.ema.tmdb.utils.Constants.IMAGE_URL
import com.ema.tmdb.utils.Constants.NOW_PLAYING
import com.ema.tmdb.utils.Constants.POPULAR
import com.ema.tmdb.utils.Constants.TOP_RATED
import com.ema.tmdb.utils.Constants.UP_COMING
import com.ema.tmdb.utils.MyNetworkRequest
import com.ema.tmdb.utils.NetworkResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(private val res: MyResource) : MainRepo {

    override suspend fun callPopularMovies(): MyNetworkRequest<ResponsePopularMovies> {
        val response = res.callPopularMovies(1)
        response.body()?.results?.map {
            res.insertMovie(
                EntityMovies(
                    it.id!!,
                    IMAGE_URL + it.posterPath,
                    it.voteAverage,
                    it.releaseDate,
                    it.originalTitle,
                    POPULAR
                )
            )

        }

        return NetworkResponse(response).generalNetworkResponse()
    }

    override suspend fun callNowPlayingMovies(): MyNetworkRequest<ResponseNowPlaying> {
        val response = res.callNowPlayingMovie(3)
        response.body()?.results?.map {
            res.insertMovie(
                EntityMovies(
                    it.id!!,
                    IMAGE_URL + it.posterPath,
                    it.voteAverage,
                    it.releaseDate,
                    it.originalTitle,
                    NOW_PLAYING
                )
            )
        }
        return if (response.isSuccessful) {
            NetworkResponse(response).generalNetworkResponse()
        } else {
            NetworkResponse(res.callNowPlayingMovie(1)).generalNetworkResponse()
        }
    }

    override suspend fun callTopRated(): MyNetworkRequest<ResponseTopRatedMovies> {
        val response = res.callTopRatedMovies(1)
        response.body()?.results?.map {
            res.insertMovie(
                EntityMovies(
                    it.id!!,
                    IMAGE_URL + it.posterPath,
                    it.voteAverage,
                    it.releaseDate,
                    it.originalTitle,
                    TOP_RATED
                )
            )
        }
        return NetworkResponse(response).generalNetworkResponse()
    }

    override suspend fun callUpComingMovies(): MyNetworkRequest<ResponseUpComingMovies> {
        val response = res.callUpComingMovies(1)
        response.body()?.results?.map {
            res.insertMovie(
                EntityMovies(
                    it.id!!,
                    IMAGE_URL + it.posterPath,
                    it.voteAverage,
                    it.releaseDate,
                    it.originalTitle,
                    UP_COMING
                )
            )
        }
        return NetworkResponse(response).generalNetworkResponse()
    }

    override fun getPopularMovies(): Flow<List<EntityMovies>> = res.getPopularMovies()

    override fun getTopRatedMovies(): Flow<List<EntityMovies>> = res.getTopRatedMovies()

    override fun getNowPlayingMovies(): Flow<List<EntityMovies>> = res.getNowPlayingMovies()
    override fun getUpComingMovies(): Flow<List<EntityMovies>> = res.getUpComingMovies()

}