package com.ema.tmdb.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ema.tmdb.data.model.network.ResponseTopRatedMovies.Result
import com.ema.tmdb.data.repository.toprated.TopRatedVideosRepo
import javax.inject.Inject

class PagingTopRatedMoviesSource @Inject constructor(private val repo: TopRatedVideosRepo, private val errorMessage: MutableLiveData<String>) :
    PagingSource<Int, Result>() {
    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        return try {
            val currentPage = params.key ?: 1
            val response = repo.callTopRatedMovies(currentPage)

            if (response.isSuccessful) {
                val topRatedMovies = response.body()!!.results ?: emptyList()
                val responseTopRatedMovies = mutableListOf<Result>()
                responseTopRatedMovies.addAll(topRatedMovies)
                LoadResult.Page(
                    data = responseTopRatedMovies,
                    prevKey = if (currentPage == 1) null else currentPage.minus(1),
                    nextKey = if (response.body()!!.results!!.isNotEmpty()) currentPage.plus(1) else null
                )
            } else {
                errorMessage.postValue(response.message())
                LoadResult.Error(Throwable(response.message()))

            }
        } catch (e: Exception) {
            errorMessage.postValue(e.message)
            LoadResult.Error(Throwable(e.message))
        }
    }
}