package com.ema.tmdb.data.network

import com.ema.tmdb.data.model.ResponseAuth
import com.ema.tmdb.data.model.network.*
import com.ema.tmdb.utils.Constants.API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {
    //https://developers.themoviedb.org/3/authentication/create-session
    //https://www.themoviedb.org/documentation/api/status-codes
    @GET("authentication/token/new")
    suspend fun authenticate(@Query(API_KEY) apiKey: String): Response<ResponseAuth>

    //movie
    //popular
    @GET("movie/popular")
    suspend fun getPopularMovie(
        @Query(API_KEY) apiKey: String,
        @Query("page") page: Int
    ): Response<ResponsePopularMovies>


    @GET("movie/{movie_id}")
    suspend fun getPopularDetail(
        @Path("movie_id") id: Int,
        @Query(API_KEY) apiKey: String
    ): Response<ResponsePopularDetail>

    //now playing
    @GET("movie/now_playing")
    suspend fun getNowPlayingMovie(
        @Query(API_KEY) apiKey: String,
        @Query("page") page: Int
    ): Response<ResponseNowPlaying>


    //top rated
    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query(API_KEY) apiKey: String,
        @Query("page") page: Int
    ): Response<ResponseTopRatedMovies>
    //up coming
    @GET("movie/upcoming")
    suspend fun getUpComingMovies(
        @Query(API_KEY) apiKey: String,
        @Query("page") page: Int
    ): Response<ResponseUpComingMovies>
}