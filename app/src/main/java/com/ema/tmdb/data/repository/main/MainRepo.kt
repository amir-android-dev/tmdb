package com.ema.tmdb.data.repository.main


import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.data.model.network.ResponseNowPlaying
import com.ema.tmdb.data.model.network.ResponsePopularMovies
import com.ema.tmdb.data.model.network.ResponseTopRatedMovies
import com.ema.tmdb.data.model.network.ResponseUpComingMovies
import com.ema.tmdb.utils.MyNetworkRequest
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface MainRepo {

    suspend fun callPopularMovies(): MyNetworkRequest<ResponsePopularMovies>
    suspend fun callNowPlayingMovies(): MyNetworkRequest<ResponseNowPlaying>
    suspend fun callTopRated(): MyNetworkRequest<ResponseTopRatedMovies>
    suspend fun callUpComingMovies(): MyNetworkRequest<ResponseUpComingMovies>

    //db
    fun getPopularMovies(): Flow<List<EntityMovies>>

    fun getTopRatedMovies(): Flow<List<EntityMovies>>

    fun getNowPlayingMovies(): Flow<List<EntityMovies>>
    fun getUpComingMovies(): Flow<List<EntityMovies>>

}