package com.ema.tmdb.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ema.tmdb.data.model.network.ResponsePopularMovies.Result
import com.ema.tmdb.data.repository.popularmovie.PopularMovieRepo
import javax.inject.Inject


class PagingPopularSource @Inject constructor(private val repo: PopularMovieRepo) :
    PagingSource<Int, Result>() {
    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        return try {
            val currentPage = params.key ?: 1
            val response = repo.callPopularMovies(currentPage)
            val popularMovies = response.body()!!.results ?: emptyList()
            val responsePopularMovies = mutableListOf<Result>()
            responsePopularMovies.addAll(popularMovies)

            if (response.isSuccessful) {
                LoadResult.Page(
                    data = responsePopularMovies,
                    prevKey = if (currentPage == 1) null else currentPage.minus(1),
                    nextKey = if (response.body()!!.results!!.isNotEmpty()) currentPage.plus(1) else null
                )
            } else {
                LoadResult.Error(Throwable(response.message()))
            }
        } catch (e: Exception) {
            LoadResult.Error(Throwable(e.message))
        }
    }

}