package com.ema.tmdb.data.model.network


import com.google.gson.annotations.SerializedName

data class ResponseTopRatedMovies(
    @SerializedName("page")
    val page: Int?, // 1
    @SerializedName("results")
    val results: List<Result>?,
    @SerializedName("total_pages")
    val totalPages: Int?, // 553
    @SerializedName("total_results")
    val totalResults: Int? // 11046
) {
    data class Result(
        @SerializedName("adult")
        val adult: Boolean?, // false
        @SerializedName("backdrop_path")
        val backdropPath: String?, // /tmU7GeKVybMWFButWEGl2M4GeiP.jpg
        @SerializedName("genre_ids")
        val genreIds: List<Int?>?,
        @SerializedName("id")
        val id: Int?, // 238
        @SerializedName("original_language")
        val originalLanguage: String?, // en
        @SerializedName("original_title")
        val originalTitle: String?, // The Godfather
        @SerializedName("overview")
        val overview: String?, // Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.
        @SerializedName("popularity")
        val popularity: Double?, // 102.666
        @SerializedName("poster_path")
        val posterPath: String?, // /3bhkrj58Vtu7enYsRolD1fZdja1.jpg
        @SerializedName("release_date")
        val releaseDate: String?, // 1972-03-14
        @SerializedName("title")
        val title: String?, // The Godfather
        @SerializedName("video")
        val video: Boolean?, // false
        @SerializedName("vote_average")
        val voteAverage: Double?, // 8.7
        @SerializedName("vote_count")
        val voteCount: Int? // 17845
    )
}