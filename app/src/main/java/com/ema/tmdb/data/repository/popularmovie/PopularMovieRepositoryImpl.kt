package com.ema.tmdb.data.repository.popularmovie


import com.ema.tmdb.data.resource.MyResource
import javax.inject.Inject

class PopularMovieRepositoryImpl @Inject constructor(private val res: MyResource) :
    PopularMovieRepo {

    override suspend fun callPopularMovies(page: Int) =
        res.callPopularMovies(page)

    override suspend fun callPopularDetail(id: Int) = res.callPopularDetail(id)


}