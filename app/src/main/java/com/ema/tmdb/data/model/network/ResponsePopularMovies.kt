package com.ema.tmdb.data.model.network


import com.google.gson.annotations.SerializedName

data class ResponsePopularMovies(
    @SerializedName("page")
    val page: Int?, // 1
    @SerializedName("results")
    val results: List<Result>?,
    @SerializedName("total_pages")
    val totalPages: Int?, // 38030
    @SerializedName("total_results")
    val totalResults: Int? // 760585
) {
    data class Result(
        @SerializedName("adult")
        val adult: Boolean?, // false
        @SerializedName("backdrop_path")
        val backdropPath: String?, // /gMJngTNfaqCSCqGD4y8lVMZXKDn.jpg
        @SerializedName("genre_ids")
        val genreIds: List<Int?>?,
        @SerializedName("id")
        val id: Int?, // 640146
        @SerializedName("original_language")
        val originalLanguage: String?, // en
        @SerializedName("original_title")
        val originalTitle: String?, // Ant-Man and the Wasp: Quantumania
        @SerializedName("overview")
        val overview: String?, // Super-Hero partners Scott Lang and Hope van Dyne, along with with Hope's parents Janet van Dyne and Hank Pym, and Scott's daughter Cassie Lang, find themselves exploring the Quantum Realm, interacting with strange new creatures and embarking on an adventure that will push them beyond the limits of what they thought possible.
        @SerializedName("popularity")
        val popularity: Double?, // 8567.865
        @SerializedName("poster_path")
        val posterPath: String?, // /ngl2FKBlU4fhbdsrtdom9LVLBXw.jpg
        @SerializedName("release_date")
        val releaseDate: String?, // 2023-02-15
        @SerializedName("title")
        val title: String?, // Ant-Man and the Wasp: Quantumania
        @SerializedName("video")
        val video: Boolean?, // false
        @SerializedName("vote_average")
        val voteAverage: Double?, // 6.5
        @SerializedName("vote_count")
        val voteCount: Int? // 1886
    )
}