package com.ema.tmdb.data.local

import androidx.room.*
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.utils.Constants.CATEGORY
import com.ema.tmdb.utils.Constants.NOW_PLAYING
import com.ema.tmdb.utils.Constants.POPULAR
import com.ema.tmdb.utils.Constants.TABLE_FAVORITE_MOVIES
import com.ema.tmdb.utils.Constants.TABLE_MOVIE
import com.ema.tmdb.utils.Constants.TOP_RATED
import com.ema.tmdb.utils.Constants.UP_COMING
import kotlinx.coroutines.flow.Flow

@Dao
interface DaoTMDB {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMovie(entity: EntityMovies)

    @Query("SELECT * FROM $TABLE_MOVIE WHERE $CATEGORY = '$POPULAR'")
    fun getPopularMovies(): Flow<List<EntityMovies>>

    @Query("SELECT * FROM $TABLE_MOVIE WHERE $CATEGORY = '$TOP_RATED'")
    fun getTopRatedMovies(): Flow<List<EntityMovies>>

    @Query("SELECT * FROM $TABLE_MOVIE WHERE $CATEGORY = '$NOW_PLAYING'")
    fun getNowPlayingMovies(): Flow<List<EntityMovies>>

    @Query("SELECT * FROM $TABLE_MOVIE WHERE $CATEGORY = '$UP_COMING'")
    fun getUpComingMovies(): Flow<List<EntityMovies>>

    //favorite
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavorite(entityFavoriteMovies: EntityFavoriteMovies)

    @Delete
    suspend fun deleteFavorite(entityFavoriteMovies: EntityFavoriteMovies)

    @Query("select * from $TABLE_FAVORITE_MOVIES")
    fun getFavoriteMovies(): Flow<List<EntityFavoriteMovies>>

    @Query("SELECT EXISTS (SELECT 1 FROM $TABLE_FAVORITE_MOVIES WHERE id = :id)")
    fun existsFavorite(id: Int): Flow<Boolean>

    @Query("delete from $TABLE_FAVORITE_MOVIES where id = :id")
    suspend fun deleteFavorite(id: Int)


}