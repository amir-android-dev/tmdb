package com.ema.tmdb.data.repository.favorite

import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.data.resource.MyResource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FavoriteRepositoryImpl @Inject constructor(private val res: MyResource) : FavoriteRepo {
    override suspend fun insertFavoriteMovie(entity: EntityFavoriteMovies) =
        res.insertFavoriteMovie(entity)

    override fun getFavoriteMovies(): Flow<List<EntityFavoriteMovies>> = res.getFavoriteMovies()

    override fun existsFavorite(id: Int): Flow<Boolean> = res.existsFavorite(id)

    override suspend fun deleteFavorite(id: Int) = res.deleteFavorite(id)
}