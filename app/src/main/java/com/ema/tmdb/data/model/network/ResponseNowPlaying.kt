package com.ema.tmdb.data.model.network


import com.google.gson.annotations.SerializedName

data class ResponseNowPlaying(
    @SerializedName("dates")
    val dates: Dates?,
    @SerializedName("page")
    val page: Int?, // 1
    @SerializedName("results")
    val results: List<Result>?,
    @SerializedName("total_pages")
    val totalPages: Int?, // 87
    @SerializedName("total_results")
    val totalResults: Int? // 1736
) {
    data class Dates(
        @SerializedName("maximum")
        val maximum: String?, // 2023-05-09
        @SerializedName("minimum")
        val minimum: String? // 2023-03-22
    )

    data class Result(
        @SerializedName("adult")
        val adult: Boolean?, // false
        @SerializedName("backdrop_path")
        val backdropPath: String?, // /nDxJJyA5giRhXx96q1sWbOUjMBI.jpg
        @SerializedName("genre_ids")
        val genreIds: List<Int?>?,
        @SerializedName("id")
        val id: Int?, // 594767
        @SerializedName("original_language")
        val originalLanguage: String?, // en
        @SerializedName("original_title")
        val originalTitle: String?, // Shazam! Fury of the Gods
        @SerializedName("overview")
        val overview: String?, // Billy Batson and his foster siblings, who transform into superheroes by saying "Shazam!", are forced to get back into action and fight the Daughters of Atlas, who they must stop from using a weapon that could destroy the world.
        @SerializedName("popularity")
        val popularity: Double?, // 3887.11
        @SerializedName("poster_path")
        val posterPath: String?, // /2VK4d3mqqTc7LVZLnLPeRiPaJ71.jpg
        @SerializedName("release_date")
        val releaseDate: String?, // 2023-03-15
        @SerializedName("title")
        val title: String?, // Shazam! Fury of the Gods
        @SerializedName("video")
        val video: Boolean?, // false
        @SerializedName("vote_average")
        val voteAverage: Double?, // 6.8
        @SerializedName("vote_count")
        val voteCount: Int? // 1395
    )
}