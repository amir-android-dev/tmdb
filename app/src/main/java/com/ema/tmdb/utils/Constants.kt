package com.ema.tmdb.utils

object Constants {
    const val API_KEY_VALUE = "db9eab47d44fb9bef402dab35d4d8283"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_URL = "https://image.tmdb.org/t/p/w500/"
    const val API_KEY = "api_key"
    //db
    const val TABLE_MOVIE = "table_movie"
    const val TABLE_FAVORITE_MOVIES = "table_favorite_movies"
    const val TMDB_DATABASE = "tmdb_database"
    const val POPULAR = "Popular"
    const val NOW_PLAYING = "Now Playing"
    const val TOP_RATED = "Top Rated"
    const val CATEGORY = "category"
    const val UP_COMING = "Up Coming"

}