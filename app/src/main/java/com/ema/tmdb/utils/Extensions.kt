package com.ema.tmdb.utils

import android.annotation.SuppressLint
import android.view.View
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.time.LocalDate

fun View.displaySnackBar(msg: String) {
    Snackbar.make(this, msg, Snackbar.LENGTH_LONG).show()
}

fun getEmojisByUnicode(): String {
    return String(Character.toChars(0x25cf))
}

fun Int.minToHour(): String {
    val time: String
    val hours = this / 60
    val minutes = this % 60
    time = if (hours > 0) "${hours}h:${minutes}min" else "${minutes}min"
    return time
}

fun RecyclerView.setUpRecycler(
    layoutManager: RecyclerView.LayoutManager,
    adapter: RecyclerView.Adapter<*>
) {
    this.layoutManager = layoutManager
    this.adapter = adapter
}

fun Double.doubleToPercent(): Int {
    return (this * 10).toInt()
}

@SuppressLint("SimpleDateFormat")
fun dateFormatter(date: String) {
    val formatter = SimpleDateFormat("yyyy-MM-dd")
    val maximumDate = formatter.parse(date)
}


