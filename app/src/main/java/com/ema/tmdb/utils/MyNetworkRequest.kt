package com.ema.tmdb.utils

sealed class MyNetworkRequest<T>(val data: T? = null, val msg: String? = null) {
    class Loading<T> : MyNetworkRequest<T>()
    class Success<T>(data: T?) : MyNetworkRequest<T>(data)
    class Error<T>(msg: String, data: T? = null) : MyNetworkRequest<T>(data, msg)
}
