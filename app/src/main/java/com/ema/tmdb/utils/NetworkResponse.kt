package com.ema.tmdb.utils


import retrofit2.Response


open class NetworkResponse<T>(private val response: Response<T>) {


    fun generalNetworkResponse(): MyNetworkRequest<T> {

        return when {
            response.message().contains("timeout") -> MyNetworkRequest.Error("Timeout")
            response.code() == 1 -> MyNetworkRequest.Error("")
            response.code() == 2 -> MyNetworkRequest.Error("")
            response.code() == 3 -> MyNetworkRequest.Error("")
            response.code() == 4 -> MyNetworkRequest.Error("")
            response.code() == 5 -> MyNetworkRequest.Error("")
            response.code() == 7 -> MyNetworkRequest.Error("")
            response.code() == 8 -> MyNetworkRequest.Error("")
            response.code() == 9 -> MyNetworkRequest.Error("")
            response.code() == 10 -> MyNetworkRequest.Error("")
            response.code() == 11 -> MyNetworkRequest.Error("")
            response.code() == 12 -> MyNetworkRequest.Error("")
            response.code() == 13 -> MyNetworkRequest.Error("")
            response.code() == 14 -> MyNetworkRequest.Error("")
            response.code() == 15 -> MyNetworkRequest.Error("")
            response.code() == 16 -> MyNetworkRequest.Error("")
            response.code() == 17 -> MyNetworkRequest.Error("")
            response.code() == 18 -> MyNetworkRequest.Error("")
            response.code() == 19 -> MyNetworkRequest.Error("")
            response.code() == 20 -> MyNetworkRequest.Error("")
            response.code() == 21 -> MyNetworkRequest.Error("")
            response.code() == 22 -> MyNetworkRequest.Error("")
            response.code() == 23 -> MyNetworkRequest.Error("")
            response.code() == 24 -> MyNetworkRequest.Error("")
            response.code() == 25 -> MyNetworkRequest.Error("")
            response.code() == 26 -> MyNetworkRequest.Error("")
            response.code() == 27 -> MyNetworkRequest.Error("")
            response.code() == 28 -> MyNetworkRequest.Error("")
            response.code() == 29 -> MyNetworkRequest.Error("")
            response.code() == 30 -> MyNetworkRequest.Error("")
            response.code() == 31 -> MyNetworkRequest.Error("")
            response.code() == 32 -> MyNetworkRequest.Error("")
            response.code() == 33 -> MyNetworkRequest.Error("")
            response.code() == 34 -> MyNetworkRequest.Error("")
            response.code() == 35 -> MyNetworkRequest.Error("")
            response.code() == 36 -> MyNetworkRequest.Error("")
            response.code() == 37 -> MyNetworkRequest.Error("")
            response.code() == 38 -> MyNetworkRequest.Error("")
            response.code() == 39 -> MyNetworkRequest.Error("")
            response.code() == 40 -> MyNetworkRequest.Error("")
            response.code() == 41 -> MyNetworkRequest.Error("")
            response.code() == 42 -> MyNetworkRequest.Error("")
            response.code() == 43 -> MyNetworkRequest.Error("")
            response.code() == 44 -> MyNetworkRequest.Error("")
            response.code() == 45 -> MyNetworkRequest.Error("")
            response.code() == 46 -> MyNetworkRequest.Error("")
            response.code() == 47 -> MyNetworkRequest.Error("")
            response.isSuccessful -> MyNetworkRequest.Success(response.body())

            else -> MyNetworkRequest.Error(response.message())
        }
    }


}
