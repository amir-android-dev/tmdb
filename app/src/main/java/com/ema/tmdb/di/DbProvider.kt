package com.ema.tmdb.di

import android.content.Context
import androidx.room.Room
import com.ema.tmdb.data.local.DatabaseTMDB
import com.ema.tmdb.utils.Constants.TMDB_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DbProvider {
    @Provides
    @Singleton
    fun dao(databaseTMDB: DatabaseTMDB) = databaseTMDB.dao()

    @Provides
    @Singleton
    fun databaseProvider(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, DatabaseTMDB::class.java, TMDB_DATABASE)
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
}