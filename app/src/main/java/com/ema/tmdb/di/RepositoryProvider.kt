package com.ema.tmdb.di

import com.ema.tmdb.data.local.DaoTMDB
import com.ema.tmdb.data.network.ApiServices
import com.ema.tmdb.data.repository.favorite.FavoriteRepo
import com.ema.tmdb.data.repository.favorite.FavoriteRepositoryImpl
import com.ema.tmdb.data.repository.main.MainRepo
import com.ema.tmdb.data.repository.main.MainRepositoryImpl
import com.ema.tmdb.data.repository.nowplaying.NowPlayingRepo
import com.ema.tmdb.data.repository.nowplaying.NowPlayingRepoImpl
import com.ema.tmdb.data.repository.popularmovie.PopularMovieRepo
import com.ema.tmdb.data.repository.popularmovie.PopularMovieRepositoryImpl
import com.ema.tmdb.data.repository.toprated.TopRatedVideosRepo
import com.ema.tmdb.data.repository.toprated.TopRatedVideosRepositoryImpl
import com.ema.tmdb.data.resource.MyResource
import com.ema.tmdb.data.resource.ResourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryProvider {
    //popular
    @Provides
    @Singleton
    fun resourceProvider(apiServices: ApiServices, dao: DaoTMDB): MyResource {
        return ResourceImpl(apiServices, dao)
    }

    @Provides
    @Singleton
    fun movieProvider(resource: MyResource): PopularMovieRepo {
        return PopularMovieRepositoryImpl(resource)
    }

    //now playing
    @Provides
    @Singleton
    fun nowPlayingProvider(resource: MyResource): NowPlayingRepo {
        return NowPlayingRepoImpl(resource)
    }

    //top rated
    @Provides
    @Singleton
    fun topRatedMoviesProvider(resource: MyResource): TopRatedVideosRepo {
        return TopRatedVideosRepositoryImpl(resource)
    }

    //main
    @Provides
    @Singleton
    fun mainProvider(resource: MyResource): MainRepo {
        return MainRepositoryImpl(resource)
    }

    //favorite
    @Provides
    @Singleton
    fun favoriteProvider(resource: MyResource): FavoriteRepo {
        return FavoriteRepositoryImpl(resource)
    }
}