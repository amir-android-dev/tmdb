package com.ema.tmdb.ui.movie.popular

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.ema.tmdb.data.model.network.ResponsePopularDetail.Genre
import com.ema.tmdb.databinding.ItemGenreBinding
import javax.inject.Inject

class AdapterDetailGenre @Inject constructor() :
    ListAdapter<Genre, AdapterDetailGenre.GenreViewHolder>(object : DiffUtil.ItemCallback<Genre>() {
        override fun areItemsTheSame(oldItem: Genre, newItem: Genre) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Genre, newItem: Genre) = oldItem == newItem
    }) {
    private lateinit var binding: ItemGenreBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        binding = ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GenreViewHolder()
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class GenreViewHolder : ViewHolder(binding.root) {
        fun bind(item: Genre) {
            binding.tvGenre.text = item.name
        }

    }

}