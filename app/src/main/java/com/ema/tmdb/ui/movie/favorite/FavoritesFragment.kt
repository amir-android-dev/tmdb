package com.ema.tmdb.ui.movie.favorite

import SwipeToDeleteCallback
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ema.tmdb.databinding.FragmentFavoritesBinding
import com.ema.tmdb.utils.Constants
import com.ema.tmdb.utils.setUpRecycler
import com.ema.tmdb.viewmodel.movie.FavoriteViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoritesFragment : Fragment() {
    private var _binding: FragmentFavoritesBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: FavoriteViewModel by viewModels()

    @Inject
    lateinit var adapter: AdapterFavorite
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentFavoritesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getAllFavoriteMovies.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.clEmpty.visibility = View.VISIBLE
                Log.e("fVORITE", "EMPTY")
            } else {
                Log.e("fVORITE", " not EMPTY")
                binding.rvFavorite.setUpRecycler(
                    LinearLayoutManager(
                        requireContext(),
                        LinearLayoutManager.VERTICAL,
                        false
                    ), adapter
                )
                adapter.submitList(it)
            }
        }
        //delete
        swipeToDelete()
        //navigate
        adapter.setOnClickListener { id, category ->
            when (category) {
                Constants.POPULAR -> {
                    navigate(FavoritesFragmentDirections.toPopularDetailFragment(id))
                }
                Constants.TOP_RATED -> {
                    navigate(FavoritesFragmentDirections.toTopRatedDetailFragment(id))
                }
                Constants.NOW_PLAYING -> {
                    navigate(FavoritesFragmentDirections.toNowPlayingDetailFragment(id))
                }
            }
        }
    }

    private fun navigate(direction: NavDirections) {
        findNavController().navigate(direction)
    }

    private fun swipeToDelete() {
        val swipeToDeleteCallback = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val itemToDelete = adapter.currentList[viewHolder.absoluteAdapterPosition]
                viewModel.deleteFavorite(itemToDelete.id)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(binding.rvFavorite)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}