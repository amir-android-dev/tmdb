package com.ema.tmdb.ui.movie.toprated

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.ema.tmdb.databinding.FragmentTopRatedBinding
import com.ema.tmdb.utils.displaySnackBar
import com.ema.tmdb.utils.setUpRecycler
import com.ema.tmdb.viewmodel.movie.TopRatedMoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class TopRatedFragment : Fragment() {
    private var _binding: FragmentTopRatedBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: TopRatedMoviesViewModel by viewModels()

    @Inject
    lateinit var adapter: AdapterTopRated

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentTopRatedBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.errorMessage.observe(viewLifecycleOwner) { errorMessage ->
            if (errorMessage != null) {
                requireView().displaySnackBar(errorMessage)
                // viewModel.errorMessage.value = null
            }
        }
        loadTopRatedMovies()

    }

    private fun loadTopRatedMovies() {
        //catch data
        lifecycleScope.launch(Dispatchers.IO) {
            viewModel.topRatedMovies.collect {
                adapter.submitData(it)
            }
        }
        //progressbar loading
        lifecycleScope.launch {
            adapter.loadStateFlow.collect {
                val state = it.refresh
                binding.progressbar.isVisible = state is LoadState.Loading
            }
        }
        //setup recycler
        binding.rvTopRated.setUpRecycler(GridLayoutManager(requireContext(), 2), adapter)

        //swipe
        binding.swiping.setOnRefreshListener {
            binding.swiping.isRefreshing = false
            adapter.refresh()
        }

        adapter.setOnClickListener {
            val action =
                TopRatedFragmentDirections.toTopRatedDetailFragment(it)
            findNavController().navigate(action)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
