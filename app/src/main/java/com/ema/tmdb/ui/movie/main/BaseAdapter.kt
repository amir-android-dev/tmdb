package com.ema.tmdb.ui.movie.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.ema.tmdb.data.model.network.*
import com.ema.tmdb.databinding.ItemMainBinding
import com.ema.tmdb.utils.Constants.IMAGE_URL
import com.ema.tmdb.utils.doubleToPercent

class BaseAdapter<T : BaseMovieResponse>(
    private val onItemClick: ((Int) -> Unit)? = null
) : ListAdapter<T, BaseViewHolder>(object : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem.id == newItem.id

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemMainBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BaseViewHolder(binding, onItemClick)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class BaseViewHolder(
    private val binding: ItemMainBinding,
    private val onItemClick: ((Int) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: BaseMovieResponse?) {
        // Bind common properties
        binding.ivPopular.load(IMAGE_URL + item?.posterPath)
        val voteInPercent = item?.voteAverage?.doubleToPercent() ?: 0
        binding.progressRate.progress = voteInPercent
        binding.tvRate.text = "$voteInPercent%"
        binding.tvPublishingDate.text = item?.releaseDate
        binding.tvMovieName.text = item?.title
        binding.root.setOnClickListener {
            item?.id?.let { id -> onItemClick?.invoke(id) }
        }

        // Bind properties specific to each response type
//        when (item) {
//            is ResponsesNowPlaying -> {
//                item.id = ResponseNowPlaying.Result.id
//            }
//            is ResponsesPopularMovies -> {
//                // bind specific properties for ResponsePopularMovies
//            }
//            // add cases for any other data classes you want to bind to the adapter
//            is ResponsesTopRatedMovies -> {
//
//
//            }
//            else -> {}
//        }
    }
}
