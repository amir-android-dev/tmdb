package com.ema.tmdb.ui.movie.main


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.databinding.ItemMainBinding
import com.ema.tmdb.utils.Constants.IMAGE_URL
import com.ema.tmdb.utils.doubleToPercent
import javax.inject.Inject


class AdapterMain @Inject constructor() :
    ListAdapter<EntityMovies, AdapterMain.MainViewHolder>(object :
        DiffUtil.ItemCallback<EntityMovies>() {
        override fun areItemsTheSame(oldItem: EntityMovies, newItem: EntityMovies) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: EntityMovies, newItem: EntityMovies) = oldItem == newItem
    }) {

    private lateinit var binding: ItemMainBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        binding = ItemMainBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainViewHolder()
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun getItemViewType(position: Int) = position


    inner class MainViewHolder : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: EntityMovies) {
            val imageUrl = IMAGE_URL + item.posterPath
            binding.ivPopular.load(imageUrl)
            val voteInPercent = item.voteAverage!!.doubleToPercent()
            //rating
            binding.progressRate.progress = voteInPercent
            //text
            binding.tvRate.text = "$voteInPercent%"
            binding.tvPublishingDate.text = item.releaseDate.toString()
            binding.tvMovieName.text = item.originalTitle.toString()
            //click
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item.id) }
            }
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null
    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

}