package com.ema.tmdb.ui.movie.popular


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.ema.tmdb.data.model.network.ResponsePopularMovies.Result
import com.ema.tmdb.databinding.ItemPopularBinding
import com.ema.tmdb.utils.Constants.IMAGE_URL
import com.ema.tmdb.utils.doubleToPercent
import javax.inject.Inject


class AdapterPopular @Inject constructor() :
    PagingDataAdapter<Result, AdapterPopular.PopularViewHolder>(object :
        DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Result, newItem: Result) = oldItem == newItem
    }) {

    private lateinit var binding: ItemPopularBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularViewHolder {
        binding = ItemPopularBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PopularViewHolder()
    }

    override fun onBindViewHolder(holder: PopularViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun getItemViewType(position: Int) = position


    inner class PopularViewHolder : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: Result) {
            val imageUrl = IMAGE_URL + item.posterPath
            binding.ivPopular.load(imageUrl)
            // val voteInPercent = (item.voteAverage!! * 10).toInt()
            val voteInPercent = item.voteAverage!!.doubleToPercent()
            //rating
            binding.progressRate.progress = voteInPercent
            //text
            binding.tvRate.text = "$voteInPercent%"
            binding.tvPublishingDate.text = item.releaseDate.toString()
            binding.tvMovieName.text = item.originalTitle.toString()
//            binding.progressRate.rotation = 0F
//            binding.progressRate.isIndeterminate = false
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item.id!!) }
            }
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null
    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

}