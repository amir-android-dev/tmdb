package com.ema.tmdb.ui.movie.nowplaying

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.ema.tmdb.databinding.FragmentNowPlayingBinding
import com.ema.tmdb.utils.setUpRecycler
import com.ema.tmdb.viewmodel.movie.NowPlayingMovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class NowPlayingFragment : Fragment() {
    private var _binding: FragmentNowPlayingBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: NowPlayingMovieViewModel by viewModels()

    @Inject
    lateinit var adapter: AdapterNowPlaying
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNowPlayingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadNowPlayingMovies()
    }

    private fun loadNowPlayingMovies() {
        lifecycleScope.launch(Dispatchers.IO) {
            viewModel.nowPlayingMovieList.collect {
                adapter.submitData(it)
            }
        }
        binding.rvNowPlaying.setUpRecycler(GridLayoutManager(requireContext(), 2), adapter)

        adapter.setOnClickListener {
            val action = NowPlayingFragmentDirections.toNowPlayingDetailFragment(it)
            findNavController().navigate(action)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}