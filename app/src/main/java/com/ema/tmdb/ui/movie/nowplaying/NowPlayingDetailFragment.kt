package com.ema.tmdb.ui.movie.nowplaying

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.ema.tmdb.R
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.databinding.FragmentNowPlayingDetailBinding
import com.ema.tmdb.ui.movie.popular.AdapterDetailGenre
import com.ema.tmdb.ui.movie.toprated.TopRatedDetailFragmentArgs
import com.ema.tmdb.utils.*
import com.ema.tmdb.viewmodel.movie.DetailMovieViewModel
import com.ema.tmdb.viewmodel.movie.FavoriteViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
@AndroidEntryPoint
class NowPlayingDetailFragment : Fragment() {
    private var _binding: FragmentNowPlayingDetailBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: DetailMovieViewModel by viewModels()
    private val viewModelFavorite: FavoriteViewModel by viewModels()
    private var isFavorite = false
    private val args: TopRatedDetailFragmentArgs by navArgs()

    @Inject
    lateinit var adapterDetailGenre: AdapterDetailGenre

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNowPlayingDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.callDetail(args.id)

        viewModel.detail.observe(viewLifecycleOwner) { response ->
            when (response) {
                is MyNetworkRequest.Error -> {
                    requireView().displaySnackBar(response.msg.toString())
                }
                is MyNetworkRequest.Loading -> {}
                is MyNetworkRequest.Success -> {
                    Log.e("TAG", "${args.id} ")
                    //image
                    binding.ivMain.load(Constants.IMAGE_URL + response.data?.posterPath)
                    binding.ivMainForground.load(Constants.IMAGE_URL + response.data?.backdropPath)
                    //title
                    binding.tvMovieName.text = response.data?.originalTitle
                    //genre
                    binding.rvGenre.setUpRecycler(
                        LinearLayoutManager(
                            requireContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false
                        ), adapterDetailGenre
                    )
                    adapterDetailGenre.submitList(response.data?.genres)

                    //general info
                    binding.tvDateTimeCountry.text =
                        "${response.data?.releaseDate} (${response.data?.originalLanguage?.uppercase()}) ${getEmojisByUnicode()} ${response.data?.runtime?.minToHour()}"
                    //overview
                    binding.tvOverview.text = response.data?.overview
                    //items
                    binding.progressRate.progress = response.data?.voteAverage!!.doubleToPercent()
                    binding.tvRate.text = "${response.data.voteAverage.doubleToPercent()}%"
                    //insert
                    val entity = response.data
                    binding.ivFavorite.setOnClickListener {
                        viewModelFavorite.insertFavorite(
                            EntityFavoriteMovies(
                                entity.id!!,
                                Constants.IMAGE_URL + entity.posterPath,
                                entity.voteAverage,
                                entity.releaseDate,
                                entity.originalTitle,
                                Constants.POPULAR
                            )
                        )
                    }
                    //if existes
                    viewModelFavorite.existsFavoriteItem(entity.id!!)
                    viewModelFavorite.favoriteExists.observe(viewLifecycleOwner) {
                        if (it) {
                            isFavorite = true
                            binding.ivFavorite.setColorFilter(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.tart_orange
                                )
                            )
                        } else {
                            binding.ivFavorite.setColorFilter(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.gray
                                )
                            )
                        }
                    }
                    //insert and delete
                    binding.ivFavorite.setOnClickListener {
                        if (!isFavorite) {
                            viewModelFavorite.insertFavorite(
                                EntityFavoriteMovies(
                                    entity.id,
                                    Constants.IMAGE_URL + entity.posterPath,
                                    entity.voteAverage,
                                    entity.releaseDate,
                                    entity.originalTitle,
                                    Constants.NOW_PLAYING
                                )
                            )
                        } else {
                            viewModelFavorite.deleteFavorite(entity.id)
                        }
                    }
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}