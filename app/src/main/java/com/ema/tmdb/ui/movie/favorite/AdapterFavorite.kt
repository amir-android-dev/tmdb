package com.ema.tmdb.ui.movie.favorite

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.ema.tmdb.R
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.databinding.ItemFavoriteBinding
import com.ema.tmdb.utils.Constants.IMAGE_URL
import com.ema.tmdb.utils.doubleToPercent
import javax.inject.Inject


class AdapterFavorite @Inject constructor() :
    ListAdapter<EntityFavoriteMovies, AdapterFavorite.MainViewHolder>(object :
        DiffUtil.ItemCallback<EntityFavoriteMovies>() {
        override fun areItemsTheSame(oldItem: EntityFavoriteMovies, newItem: EntityFavoriteMovies) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: EntityFavoriteMovies,
            newItem: EntityFavoriteMovies
        ) = oldItem == newItem
    }) {

    private lateinit var binding: ItemFavoriteBinding
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        context = parent.context
        binding = ItemFavoriteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainViewHolder()
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun getItemViewType(position: Int) = position


    inner class MainViewHolder : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: EntityFavoriteMovies) {
            val imageUrl = IMAGE_URL + item.posterPath
            binding.ivPopular.load(imageUrl)
            // val voteInPercent = (item.voteAverage!! * 10).toInt()
            val voteInPercent = item.voteAverage!!.doubleToPercent()
            //rating
            binding.progressRate.progress = voteInPercent
            //text
            binding.tvRate.text = "$voteInPercent%"
            binding.tvPublishingDate.text = item.releaseDate.toString()
            binding.tvMovieName.text = item.originalTitle.toString()
            binding.ivFavorite.setColorFilter(ContextCompat.getColor(context, R.color.tart_orange))
            //navigate

            //click
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item.id,item.category) }
            }
        }
    }

    private var onItemClickListener: ((Int,String) -> Unit)? = null
    fun setOnClickListener(listener: (Int,String) -> Unit) {
        onItemClickListener = listener
    }

}