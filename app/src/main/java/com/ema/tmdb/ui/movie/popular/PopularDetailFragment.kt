package com.ema.tmdb.ui.movie.popular

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.ema.tmdb.R
import com.ema.tmdb.data.model.db.EntityFavoriteMovies
import com.ema.tmdb.databinding.FragmentPopularDetailBinding
import com.ema.tmdb.utils.*
import com.ema.tmdb.utils.Constants.IMAGE_URL
import com.ema.tmdb.utils.Constants.POPULAR
import com.ema.tmdb.viewmodel.movie.DetailMovieViewModel
import com.ema.tmdb.viewmodel.movie.FavoriteViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PopularDetailFragment : Fragment() {

    //binding
    private var _binding: FragmentPopularDetailBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: DetailMovieViewModel by viewModels()
    private val viewModelFavorite: FavoriteViewModel by viewModels()
    private var isFavorite = false
    private val args: PopularDetailFragmentArgs by navArgs()
    @Inject
    lateinit var adapterDetailGenre: AdapterDetailGenre

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPopularDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.callDetail(args.id)
        setupDetails()


    }

    @SuppressLint("SetTextI18n")
    private fun setupDetails() {
        viewModel.detail.observe(viewLifecycleOwner) {
            when (it) {
                is MyNetworkRequest.Error -> {
                    requireView().displaySnackBar(it.msg.toString())
                }
                is MyNetworkRequest.Loading -> {}
                is MyNetworkRequest.Success -> {
                    Log.e("TAG", "${args.id} ")
                    //image
                    binding.ivMain.load(IMAGE_URL + it.data?.posterPath)
                    binding.ivMainForground.load(IMAGE_URL + it.data?.backdropPath)
                    //title
                    binding.tvMovieName.text = it.data?.originalTitle
                    //genre
                    binding.rvGenre.setUpRecycler(
                        LinearLayoutManager(
                            requireContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false
                        ), adapterDetailGenre
                    )
                    adapterDetailGenre.submitList(it.data?.genres)

                    //general info
                    binding.tvDateTimeCountry.text =
                        "${it.data?.releaseDate} (${it.data?.originalLanguage?.uppercase()}) ${getEmojisByUnicode()} ${it.data?.runtime?.minToHour()}"
                    //overview
                    binding.tvOverview.text = it.data?.overview
                    //items
                    binding.progressRate.progress = it.data?.voteAverage!!.doubleToPercent()
                    binding.tvRate.text = "${it.data.voteAverage.doubleToPercent()}%"
                    //insert
                    val entity = it.data
                    binding.ivFavorite.setOnClickListener {

                    }
                    //if existes
                    viewModelFavorite.existsFavoriteItem(entity.id!!)
                    viewModelFavorite.favoriteExists.observe(viewLifecycleOwner) { exists ->
                        if (exists) {
                            isFavorite = true
                            binding.ivFavorite.setColorFilter(ContextCompat.getColor(requireContext(),
                                R.color.tart_orange))
                        } else {
                            binding.ivFavorite.setColorFilter(ContextCompat.getColor(requireContext(),
                                R.color.gray))
                        }
                    }
                    //insert and delete
                    binding.ivFavorite.setOnClickListener {
                        if(!isFavorite){
                            viewModelFavorite.insertFavorite(
                                EntityFavoriteMovies(
                                    entity.id,
                                    IMAGE_URL + entity.posterPath,
                                    entity.voteAverage,
                                    entity.releaseDate,
                                    entity.originalTitle,
                                    POPULAR
                                )
                            )
                        }else{
                            viewModelFavorite.deleteFavorite(entity.id)
                        }
                    }



                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}



