package com.ema.tmdb.ui.movie.popular


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.ema.tmdb.databinding.FragmentPopularMovieBinding
import com.ema.tmdb.viewmodel.movie.PopularMovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PopularMovieFragment : Fragment() {

    private var _binding: FragmentPopularMovieBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: PopularMovieViewModel by viewModels()

    @Inject
    lateinit var adapter: AdapterPopular
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentPopularMovieBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadPopularMovies()
    }

    private fun loadPopularMovies() {
        //collect data
        lifecycleScope.launch {
            viewModel.popularMoviesList.collect {
                adapter.submitData(it)
            }
        }
        //loading
        lifecycleScope.launch {
            adapter.loadStateFlow.collect {
                val state = it.refresh
                //   binding.progressBar.isVisible = state is LoadState.Loading
            }
        }
        //recyclerView
        binding.rvPopular.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.rvPopular.adapter = adapter
        adapter.setOnClickListener {
            val action = PopularMovieFragmentDirections.toPopularDetailFragment(it)
            findNavController().navigate(action)
        }
        //swipeRefresh
        /*  binding.swiping.setOnRefreshListener {
              binding.swiping.isRefreshing = false
              adapter.refresh()
          }*/
        //load more
        /*     binding.rvComments.adapter = adapter.withLoadStateFooter(LoadMoreAdapter {
                 adapter.retry()
             })*/
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}