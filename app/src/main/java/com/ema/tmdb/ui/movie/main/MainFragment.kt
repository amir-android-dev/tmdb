package com.ema.tmdb.ui.movie.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.ema.tmdb.data.model.db.EntityMovies
import com.ema.tmdb.databinding.FragmentMainBinding
import com.ema.tmdb.utils.CheckConnection
import com.ema.tmdb.utils.displaySnackBar

import com.ema.tmdb.utils.setUpRecycler
import com.ema.tmdb.viewmodel.movie.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    //others
    private val viewModel: MainViewModel by viewModels()

    @Inject
    lateinit var adapterMainPopular: AdapterMain

    @Inject
    lateinit var adapterMainTopRated: AdapterMain

    @Inject
    lateinit var adapterMainNowPlaying: AdapterMain

    @Inject
    lateinit var adapterUpComing: AdapterMainTeaser

    @Inject
    lateinit var checkConnection: CheckConnection

    private var autoScrollIndex = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapterMain()
        //upcoming
        viewModel.upComingMoviesDb.observe(viewLifecycleOwner) {
            adapterUpComing.submitList(it)
            autoScroller(it)
        }
        //db popular
        viewModel.popularMoviesDb.observe(viewLifecycleOwner) { dbResponse ->
            adapterMainPopular.submitList(dbResponse)
        }
        //db topRated
        viewModel.topRatedDb.observe(viewLifecycleOwner) { dbResponse ->
            adapterMainTopRated.submitList(dbResponse)
        }
        //db nowPlaying
        viewModel.nowPlayingMoviesDb.observe(viewLifecycleOwner) { dbResponse ->
            adapterMainNowPlaying.submitList(dbResponse)
        }


        //check connection
        checkConnection.observe(viewLifecycleOwner) {
            if (it) {
                viewModel.refresh()


            } else if (!it) {
                requireView().displaySnackBar("NO CONNECTION")
            }
        }
        binding.swRefresh.setOnRefreshListener {
            binding.swRefresh.isRefreshing = false
        }

        //navigate
        binding.ivMorePopular.setOnClickListener {
            navigateToSeeMore(MainFragmentDirections.actionMainFragmentToMovieFragment())
        }
        binding.ivMoreNowPlaying.setOnClickListener {
            navigateToSeeMore(MainFragmentDirections.actionMainFragmentToNowPlayingFragment())
        }
        binding.ivTopRated.setOnClickListener {
            navigateToSeeMore(MainFragmentDirections.actionMainFragmentToTopRatedFragment())
        }
        //navigate to detail
        adapterMainNowPlaying.setOnClickListener {
            navigateToSeeMore(MainFragmentDirections.toNowPlayingDetailFragment(it))
        }
        adapterMainPopular.setOnClickListener {
            navigateToSeeMore(MainFragmentDirections.toPopularDetailFragment(it))
        }
        adapterMainTopRated.setOnClickListener {
            navigateToSeeMore(MainFragmentDirections.toTopRatedDetailFragment(it))
        }


    }

    private fun navigateToSeeMore(direction: NavDirections) {
        findNavController().navigate(direction)
    }


    private fun setupAdapterMain() {
        binding.rvPopular.setUpRecycler(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            ), adapterMainPopular
        )
        binding.rvTopRated.setUpRecycler(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            ), adapterMainTopRated
        )
        binding.rvNowPlaying.setUpRecycler(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            ), adapterMainNowPlaying
        )

        //up coming
        val snapHelper = LinearSnapHelper()
        binding.rvUpcoming.setUpRecycler(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            ), adapterUpComing
        )
        //snap
        snapHelper.attachToRecyclerView(binding.rvUpcoming)
    }

    private fun autoScroller(upComingMovies: List<EntityMovies>) {
        lifecycleScope.launch {
            repeat(100) {
                delay(5000)
                if (autoScrollIndex < upComingMovies.size) {
                    autoScrollIndex += 1
                } else {
                    autoScrollIndex = 0
                }
                binding.rvUpcoming.smoothScrollToPosition(autoScrollIndex)
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}